FROM python:3-alpine

WORKDIR /code
RUN mkdir /code/db
COPY . /code

RUN pip install -r requirements.txt

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000